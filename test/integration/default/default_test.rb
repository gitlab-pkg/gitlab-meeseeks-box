# encoding: utf-8

# InSpec tests for gitlab-meeseeks-box package

control 'general-pkg-checks' do
  impact 1.0
  title 'General tests for gitlab-meeseeks-box package'
  desc "
    This control ensures that:
      * gitlab-meeseeks-box package version 0.0.30 is installed
  "
  describe package('gitlab-meeseeks-box') do
    it { should be_installed }
    its ('version') { should eq '0.0.30' }
  end
end

control 'general-service-checks' do
  impact 1.0
  title 'General tests for meeseeks-box service'
  desc '
    This control ensures that:
      * meeseeks-box service is installed, enabled and running
  '
  describe service('meeseeks-box') do
    it { should be_installed }
    it { should be_enabled }
  end

end

# control 'meeseeks-box-requirements' do
#   impact 1.0
#   title 'meeseeks-box requirements'
#   desc '
#     This control is just a collection of checks/reminders for
#     humans that will request butter. Stuff that meeseeks-box
#     expects to be on system:
#       * file with environment variables
#       * nginx key and cert
#   '
#   describe file('/etc/meeseeks-box.env') do
#     it { should be_file }
#     # owned and readable only by meeseeks
#     its('owner') {should eq 'meeseeks' }
#     its('group') {should eq 'meeseeks' }
#     its('mode') { should eq 0o400 }
#   end
# end
